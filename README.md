# List of Curated Devops References

This is a list of curated references to DevOps related matters

[[_TOC_]]

## Tools

### Automation Tools

* [doit Automation Tool](https://pydoit.org/) - Python based automation tool.  People often compare doit to tools like make, *grunt*, *rake*, *scons*, *snakemake*.
* [Y{}R](https://yor.io) - Yor is an open-source tool that automatically tags infrastructure as code (IaC) templates with attribution and ownership details, unique IDs that get carried across to cloud resources, and any other need-to-know information.


### Templating Tools

* [ytt - YAML Templating Tool](https://get-ytt.io/) - ytt is a templating tool that understands YAML structure
allowing you to focus on your data instead of how to properly escape it.
* [gucci - Simple CLI Templating Tool](https://github.com/noqcks/gucci) - A simple CLI templating tool written in golang.  It is a standalone tool supporting all of existing [golang templating functions](https://golang.org/pkg/text/template/#hdr-Functions) as well as template helpers from [sprig templating functions library](http://masterminds.github.io/sprig/)
* [Yasha - A command-line tool to render Jinja templates for great good](https://github.com/kblomqvist/yasha) - Yasha is a code generator based on Jinja2 template engine. Yasha supports custom plugins/filters, and is able to use Ansible filters/plugins.
* [gomplate](https://docs.gomplate.ca/) - gomplate is a template renderer which supports a growing list of datasources, such as: JSON (including EJSON - encrypted JSON), YAML, AWS EC2 metadata, Hashicorp Consul and Hashicorp Vault secrets.



### Security Related Tools

* [ScoutSuite](https://github.com/nccgroup/ScoutSuite) - Scout Suite is an open source multi-cloud security-auditing tool, which enables security posture assessment of cloud environments.
* [Brim](https://github.com/brimsec/brim) - Brim is an open source desktop application for security and network specialists. Brim makes it easy to search and analyze data from:
  * packet captures, like those created by Wireshark, and
  * structured logs, especially from the Zeek network analysis framework. 

#### Cryptography

* [age](https://github.com/FiloSottile/age) - A simple, modern and secure encryption tool (and Go library) with small explicit keys, no config options, and UNIX-style composability.
  * Yubikey plugin for age - https://github.com/str4d/age-plugin-yubikey
* [Minisign](https://jedisct1.github.io/minisign/) - Minisign is a dead simple tool to sign files and verify signatures. It is portable, lightweight, and uses the highly secure Ed25519 public-key signature system.
* [passage](https://github.com/FiloSottile/passage) - A fork of password-store (https://www.passwordstore.org) that uses age (https://age-encryption.org) as backend.


### SSH Management Tools

* [Teleport SSH](https://gravitational.com/teleport/) - Gravitational Teleport is a gateway for managing access to clusters of Linux servers via SSH or the Kubernetes API.
* [Pritunl Zero](https://zero.pritunl.com/) - Pritunl Zero is a zero trust system that provides secure authenticated access to servers without needing to manage authorized SSH keys.
* [Userify](https://userify.com/) - Userify is an SSH key and User Manager for the cloud and datacenters that creates user accounts for devops, engineers, developers, database administrators, etc and allows them to log in quickly and easily.
* [smallstep](https://smallstep.com/) - The easy way for teams to SSH.  Stop gathering and shipping and rotating SSH public keys for all your users and hosts. smallstep SSH makes it easy for you to use SSH certificates instead. Users sign in to your identity provider via OAuth and are issued an SSH certificate for the day. It's stored in memory, and they use it to SSH to your hosts as usual. No more user keys scattered across your infrastructure. Instead, you manage SSH and sudo access in our admin panel. And when users are removed from your identity provider, all server access is revoked immediately.

### VPN/BeyondCorp Related Tools

* [Subspace](https://github.com/subspacecommunity/subspace) - A simple WireGuard VPN server GUI.  Provide ability to add/remove devices, manage configurations and supports SSO with SAML.
* [Tailscale](https://tailscale.com/) - Private networks made easy.  Connect all your devices using WireGuard, without the hassle.
Tailscale makes it as easy as installing an app and signing in.  Provide Zero-Trust mesh end-to-end secured connections.
* [Pomerium](https://github.com/pomerium/pomerium) - Pomerium is an identity-aware proxy that enables secure access to internal applications. Pomerium provides a standardized interface to add access control to applications regardless of whether the application itself has authorization or authentication baked-in. Pomerium gateways both internal and external requests, and can be used in situations where you'd typically reach for a VPN.

### Miscellaneous Servers

* [Caddy2](https://caddyserver.com/v2) - A new kind of extensible platform for server apps.  A web server written in Go to use TLS automatically and by default.

### Python Related

* [Poetry](https://python-poetry.org/) - Python packaging and dependency management made easy
* [Brython](https://tauri.studio/en/) - Brython is designed to replace Javascript as the scripting language for the Web. As such, it is a Python 3 implementation (you can take it for a test drive through a web console), adapted to the HTML5 environment, that is to say with an interface to the DOM objects and events.

#### Python Data

* [datatable](https://github.com/h2oai/datatable) - Datatable is a python library for manipulating tabular data. It supports out-of-memory datasets, multi-threaded data processing, and flexible API. `datatable` a Python package for manipulating 2-dimensional tabular data structures (aka data frames). It is close in spirit to pandas or SFrame with specific emphasis on speed and big data support. As the name suggests, the package is closely related to R's data.table and attempts to mimic its core algorithms and API.
* [Dask](https://dask.org/) - Dask is a flexible library for parallel computing in Python. 
* [Modin](https://modin.readthedocs.io/en/latest/) - Modin is targeted toward parallelizing the entire pandas API, without exception. As the pandas API continues to evolve, so will Modin’s pandas API. Modin is intended to be used as a drop-in replacement for pandas, such that even if the API is not yet parallelized, it still works by falling back to running pandas. 
* [PySpark](https://spark.apache.org/docs/latest/api/python/index.html) - Python library for Apache Spark.



### Server Management Tools

* [Foreman](https://theforeman.org/) - Foreman is a complete lifecycle management tool for physical and virtual servers.
* [Lens](https://k8slens.dev/) - Lens is the only IDE you’ll ever need to take control of your Kubernetes clusters. It's open source and free.
* [syncthing](https://syncthing.net/) - Syncthing is a continuous file synchronization program. It synchronizes files between two or more computers in real time, safely protected from prying eyes. Your data is your data alone and you deserve to choose where it is stored, whether it is shared with some third party, and how it’s transmitted over the internet.
* [Restic](https://restic.net/) - Restic is a modern backup program that can back up your files to many different storage types.

### Kubernetes Related Tools

* [k0s](https://k0sproject.io/) - k0s is an all-inclusive Kubernetes distribution with all the required bells and whistles preconfigured to make building a Kubernetes clusters a matter of just copying an executable to every host and running it.
* [Microk8s](https://microk8s.io/) - Canonical's Zero-ops high availability. Autonomous, production-grade Kubernetes, on cloud, clusters, workstations, Edge and IoT.
* [KubeSphere](https://kubesphere.io/) - KubeSphere is a distributed operating system managing cloud native applications with Kubernetes as its kernel, and provides plug-and-play architecture for the seamless integration of third-party applications to boost its ecosystem.
* [Otterize](https://otterize.com/open-source) - An open source implementation of intent-based access control (IBAC) for a Kubernetes cluster. Automatically configure your existing access controls based on clients intents.

#### Kubernetes Links
* [Kubetools](https://collabnix.github.io/kubetools/) - A Curated List of Kubernetes Tools

### Documentation/Writing Tools

* [Jupyter{book}](https://jupyterbook.org/intro.html) - Jupyter Book is an open source project for building beautiful, publication-quality books and documents from computational material.  Based on [MyST (Markdown with full Sphinx Doc supports)](https://myst-parser.readthedocs.io/en/latest/), Sphinx and Jypter
* [bookdown](https://bookdown.org) - The bookdown package is a free and open-source R package built on top of R Markdown to make it really easy to write books and long-form articles/reports.
* [Sphinx](https://www.sphinx-doc.org) - Sphinx is a tool that makes it easy to create intelligent and beautiful documentation, written by Georg Brandl and licensed under the BSD license.
* [Antora](https://antora.org) - Antora makes it easy for tech writers to create documentation and publish it to the web. As a tech writer, you focus on authoring content in AsciiDoc, Antora’s content markup language. You organize those files into a standard project structure and store them in one or more content (git) repositories. Antora picks up your content from there and transforms it into a website. That’s all there is to it!

### Databases

* [QuestDB](https://questdb.io) - QuestDB is an open source database which makes time-series fast and easy.  QuestDB is a relational column-oriented database which can handle transactions and real-time analytics. It uses the SQL language with a few extensions for time-series. 
* [ClickHouse](https://clickhouse.tech/) - ClickHouse is a fast open-source OLAP database management system
It is column-oriented and allows to generate analytical reports using SQL queries in real-time.
* [TimescaleDB](https://www.timescale.com/) - TimescaleDB is an open-source database designed to make SQL scalable for time-series data. It is engineered up from PostgreSQL and packaged as a PostgreSQL extension, providing automatic partitioning across time and space (partitioning key), as well as full SQL support.
* [Cortex](https://cortexmetrics.io/) - Cortex: horizontally scalable, highly available, multi-tenant, long term storage for Prometheus.
* [Thanos](https://thanos.io/) - Thanos is a set of components that can be composed into a highly available metric system with unlimited storage capacity, which can be added seamlessly on top of existing Prometheus deployments.  Thanos is a CNCF Sandbox project.
  Thanos leverages the Prometheus 2.0 storage format to cost-efficiently store historical metric data in any object storage while retaining fast query latencies. Additionally, it provides a global query view across all Prometheus installations and can merge data from Prometheus HA pairs on the fly.
* [ShardingSphere](https://shardingsphere.apache.org/) - The ecosystem to transform any database into a distributed database system, and enhance it with sharding, elastic scaling, encryption features & more.


### Javascript/Typescript Related

#### Frontend

* [Alpine.js](https://github.com/alpinejs/alpine) - Alpine.js offers you the reactive and declarative nature of big frameworks like Vue or React at a much lower cost.  You get to keep your DOM, and sprinkle in behavior as you see fit.  Think of it like [Tailwind](https://tailwindcss.com/) for JavaScript.


### Miscellaneous Tools

* [direnv](https://direnv.net/) - `direnv` is an extension for your shell. It augments existing shells with a new feature that can load and unload environment variables depending on the current directory.
* [Ventoy](https://www.ventoy.net) - Ventoy is an open source tool to create bootable USB drive for ISO files.  With ventoy, you don't need to format the disk over and over, you just need to copy the iso file to the USB drive and boot it.
* [cfgdiff](https://github.com/evgeni/cfgdiff) - diff(1) is an awesome tool, you use it (or similar implementations like git diff, svn diff etc) every day when dealing with code. But configuration files aren't code. Indentation often does not matter (yeah, there is diff -w and yeah, people use YAML for configs), order of settings does not matter and comments are just beautiful noise. Supports `INI`, `JSON`, `YAML`, `XML`.
* [binenv](https://github.com/devops-works/binenv) - binenv will help you download, install and manage the binaries programs (i.e., distributions) such as kubectl, helm, Terraform, etc.
* [k6](https://k6.io/) - The best developer experience for load testing. Open source load testing tool and SaaS for engineering teams.
* [VerbalExpressions](https://verbalexpressions.github.io/) - Regular Expressions made easy.
* [Maddy](https://github.com/foxcpp/maddy) - Maddy Mail Server implements all functionality required to run a e-mail server. It can send messages via SMTP (works as MTA), accept messages via SMTP (works as MX) and store messages while providing access to them via IMAP. In addition to that it implements auxiliary protocols that are mandatory to keep email reasonably secure (DKIM, SPF, DMARC, DANE, MTA-STS).  It replaces Postfix, Dovecot, OpenDKIM, OpenSPF, OpenDMARC and more with one daemon with uniform configuration and minimal maintenance cost.
* [SOPS](https://github.com/mozilla/sops) - SOPS: Secrets OPerationS - sops is an editor of encrypted files that supports YAML, JSON, ENV, INI and BINARY formats and encrypts with AWS KMS, GCP KMS, Azure Key Vault and PGP.
* [octoDNS](https://github.com/octodns/octodns) - Tools for managing DNS across multiple providers
* [Score](https://score.dev) - Score is a developer-centric and platform-agnostic workload specification. It ensures consistent configuration between local and remote environments. And it's open source.
* [chezmoi](https://www.chezmoi.io/) - Manage your dotfiles across multiple diverse machines, securely.
* [CloudKnit](https://cloudknit.io) - Open-source progressive delivery platform for managing cloud environments. It comes with dashboards to help visualize environments and observe them.
* [mani](https://manicli.com/) - mani is a CLI tool that helps you manage multiple repositories. It's useful when you are working with microservices, multi-project systems, many libraries or just a bunch of repositories and want a central place for pulling all repositories and running commands over them. Similiar tool to `terrafile`, but for general purpose.
* [terrafile](https://github.com/coretech/terrafile) - Terrafile is a binary written in Go to systematically manage external modules from Github for use in Terraform.
* [fq](https://github.com/wader/fq) - jq for binary formats - tool, language and decoders for working with binary and text formats.


### Monitoring and Logging Related
* [Vector](https://github.com/timberio/vector) - Vector is a lightweight, ultra-fast, open-source tool for building observability pipelines. Compared to Logstash and friends, Vector improves throughput by ~10X while significantly reducing CPU and memory usage.
* [YACE](https://github.com/ivx/yet-another-cloudwatch-exporter) - AWS cloudwatch to prometheus exporter - Discovers services through AWS tags, gets cloudwatch data and provides them as prometheus metrics with AWS tags as labels.
* [Ward](https://tauri.studio/en/) - Ward is a simple and and minimalistic server monitoring tool. Ward supports adaptive design system. Also it supports dark theme. It shows only principal information and can be used, if you want to see nice looking dashboard instead looking on bunch of numbers and graphs. Ward works nice on all popular operating systems, because it uses OSHI.
* [GoAccess](https://goaccess.io/) - GoAccess is an open source real-time web log analyzer and interactive viewer that runs in a terminal in *nix systems or through your browser.  It provides fast and valuable HTTP statistics for system administrators that require a visual server report on the fly.
* [Upptime](https://upptime.js.org/) - Upptime is the open-source uptime monitor and status page, powered entirely by GitHub.


### API Related
* [APISIX](https://github.com/apache/apisix) - Apache APISIX is a dynamic, real-time, high-performance API gateway, based on the Nginx library and etcd.  
  * APISIX provides rich traffic management features such as load balancing, dynamic upstream, canary release, circuit breaking, authentication, observability, and more.  
  * You can use Apache APISIX to handle traditional north-south traffic, as well as east-west traffic between services. It can also be used as a k8s ingress controller.
* [Directus](https://github.com/directus/directus) - Directus is a real-time API and App dashboard for managing SQL database content.  Directus wraps your new or existing SQL database with a realtime GraphQL+REST API for developers, and an intuitive admin app for non-technical users.
* [PostgREST](https://github.com/PostgREST/postgrest) - PostgREST serves a fully RESTful API from any existing PostgreSQL database. It provides a cleaner, more standards-compliant, faster API than you are likely to write from scratch.
* [HTTPie](https://httpie.io/) -  HTTPie for Terminal — modern, user-friendly command-line HTTP client for the API era. JSON support, colors, sessions, downloads, plugins & more.
* [Trigger](https://trigger.dev/) - Trigger workflows from APIs, on a schedule, or on demand. API calls are easy with authentication handled for you. Add durable delays that survive server restarts. The developer-first open source Zapier alternative.


### CA Certificate Management

* [mkcert](https://github.com/FiloSottile/mkcert) - mkcert is a simple tool for making locally-trusted development certificates. It requires no configuration.
* [Smallstep Certificate Manager](https://smallstep.com/certificate-manager/) - Easily issue private TLS/SSL certificates to internal workloads using ACME, clouds via APIs, or IoT devices. Smallstep Certificate Manager is your certificate authority, managed by the experts at smallstep.

### Data Processing

* [Meltano](https://meltano.com/) - Meltano is an open source platform for building, running & orchestrating ELT pipelines built out of Singer taps and targets and dbt models, that you can run locally or easily deploy in production.  Owned by Gitlab.

#### Data Analytic Tools

* [Pandas](https://pandas.pydata.org/) - Pandas is a fast, powerful, flexible and easy to use open source data analysis and manipulation tool, built on top of the Python programming language.
* [Dask](https://docs.dask.org/en/latest/) - Dask natively scales Python.  Dask provides advanced parallelism for analytics, enabling performance at scale for the tools you love
* [Modin](http://modin.readthedocs.io/) - Modin: Speed up your Pandas workflows by changing a single line of code.  Modin uses Ray or Dask to provide an effortless way to speed up your pandas notebooks, scripts, and libraries. Unlike other distributed DataFrame libraries, Modin provides seamless integration and compatibility with existing pandas code. Even using the DataFrame constructor is identical.
* [Tabel](https://github.com/BastiaanBergman/tabel) - Lightweight, intuitive and fast data-tables. Tabel data-tables are tables with columns and column names, rows and row numbers. Indexing and slicing your data is analogous to numpy array's. The only real difference is that each column can have its own data type.

##### Notebooks

* [Jupyter](https://jupyter.org/) - The Jupyter Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text. Uses include: data cleaning and transformation, numerical simulation, statistical modeling, data visualization, machine learning, and much more.
* [Polynote](https://polynote.org/) - Polynote is a different kind of notebook. It supports mixing multiple languages in one notebook, and sharing data between them seamlessly. It encourages reproducible notebooks with its immutable data model.  Developed by Netflix.
* [Zeppelin](https://zeppelin.apache.org/) - Web-based notebook that enables data-driven, interactive data analytics and collaborative documents with SQL, Scala and more. 

### Programming Miscellaneous

* [Tauri](https://tauri.studio/en/) - Tauri is a framework for building tiny, blazing fast binaries for all major desktop platforms. Developers can integrate any front-end framework that compiles to HTML, JS and CSS for building their user interface. The backend of the application is a rust-sourced binary with an API that the front-end can interact with.
* [Cutter](https://cutter.re/) - Free and Open Source RE Platform powered by radare2.  Cutter goal is to be an advanced FREE and open-source reverse-engineering platform while keeping the user experience at mind. Cutter is created by reverse engineers for reverse engineers.


#### Frontend UI

* [tailwindcss](https://tailwindcss.com/) - Rapidly build modern websites without ever leaving your HTML.


## References/Tutorials

### Git Related 

* [Flight rules for Git](https://github.com/k88hudson/git-flight-rules) - A guide for programmers using Git - about what to do when things go wrong.

### General

* [Open Source Alternatives](https://github.com/btw-so/open-source-alternatives) - List of open-source alternatives to everyday SaaS products.
